<!DOCTYPE html>
<style type="text/css">
	img{
		max-width: 10%;
		max-height: 10%;
	}
</style>
<html>
<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" class="img-circle"></center><br>
	<div class="container">
		<hr>
		<?php
		session_start();
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "estore";
		$avatar = "default.png";
		require_once('Encryption.php');
// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		if(isset($_POST['avatar']) && !empty($_POST['avatar'])){
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["avatar"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				echo "<div class='alert alert-danger'><strong>Error!</strong>Only JPG, JPEG, PNG & GIF files are allowed.</div>";
			$uploadOk = 0;
		}
// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "<div class='alert alert-danger'><strong>Error!</strong>Your file was not uploaded.</div>";
// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {
			//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
				$avatar = basename($_FILES["avatar"]["name"]);
			} else {
				$avatar = "default.png";
			//echo "Sorry, there was an error uploading your file.";
			}
		}
	}
	if(!empty($_POST)){
		$existEmail = "SELECT * FROM users WHERE email='".$_POST['email']."'";
		if($result = mysqli_query($conn,$existEmail))
		{
			if(mysqli_num_rows($result) > 0)
			{
				$_SESSION['first'] = $_POST["firstName"];
				$_SESSION['last'] = $_POST["lastName"];
				$_SESSION['mail'] = $_POST["email"];
				$_SESSION['add'] = $_POST["address"];
				$_SESSION['phone'] = $_POST["phone"];
				header('Location: http://localhost/eStore/Register.php?mail=1');
			}
			else
			{
				$password = md5($_POST["password"]);
				$sql = 'INSERT INTO users (firstName, lastName, email, password,avatar,phone,address)
				VALUES ("'.$_POST["firstName"].'","'.$_POST["lastName"].'","'.$_POST["email"].'","'.$password.'","'.$avatar.'","20'.$_POST["phone"].'","'.$_POST['address'].'")';

				if ($conn->query($sql) === TRUE) {
					echo "<div class='alert alert-success'><strong>You are registered!</strong> Please check your email to activate your account.</div>";
					//Send cart verification mail
					require_once(dirname(__FILE__)."/vendor/swiftmailer/swiftmailer/lib/swift_required.php");
					$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
					->setUsername('jswilliam93')
					->setPassword('Fourzeroes');

					$mailer = Swift_Mailer::newInstance($transport);
					$mail = $_POST['email'];
					$converter = new Encryption;
					$encodedMail = $converter->encode($mail);

					$message = Swift_Message::newInstance('Account Activation')
					->setFrom(array('jswilliam93@gmail.com' => 'Joe William'))
					->setTo(array('jswilliam93@gmail.com'))
					->setBody('Hi '.$_POST['firstName']." ".$_POST['lastName']." - ".$_POST['email']." - ,\n". "Please confirm your email by clicking the following link \n http://localhost/eStore/activate_account.php?mail=".$encodedMail);
					$result = $mailer->send($message);
				} else {
					header('Refresh:0; url: http://localhost/eStore/Register.php?mail=1');
				}
			}
		}
		else
			header('Refresh:0; url: http://localhost/eStore/Register.php');
	}
	$conn->close();
	?>
	<?php if(!empty($_GET['error'])) echo "<div class='alert alert-danger'><strong>Error!</strong> Incorrect Email or Password</div>"; ?>
	<?php if(!empty($_GET['guest'])) echo "<div class='alert alert-warning'><strong>You're not allowed!</strong> Please Login</div>"; ?>

	<form role="form" action="Products.php" method="post">
		<div class="col-md-6">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" name="email" required value='<?php if(!empty($_POST["email"])) echo $_POST["email"]?>'>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="pwd">Password</label>
				<input type="password" class="form-control" name="password" required value='<?php if(!empty($_POST["password"])) echo $_POST["password"]?>'>
			</div>
		</div>
		<center><button type="submit" class="btn btn-primary">Login</button></center><br>
		<a href="register.php"><p align="right">New here? Sign up.</p></a>
		<a href="products.php"><p align="right">In hurry? Check our products.</p></a>
	</form>
</div>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
