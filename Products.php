<!DOCTYPE html>
<?php session_start();  ?>
<style type="text/css">
	#logo{
		max-width: 10%;
		max-height: 10%;
	}
	#product{
		max-width: 150px;
		max-height: 150px;
	}
</style>
<html>
<head>
	<title>Products</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "estore";

// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
			echo "error con";
	}
	if(isset($_POST) && !empty($_POST)){
		$password = md5($_POST["password"]);
		$sql = 'SELECT * FROM users WHERE email="'.$_POST['email'].'" and password ="'.$password.'"';
		if ($result=mysqli_query($conn,$sql)) {
			if(mysqli_num_rows($result) > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$_SESSION["idUsers"] = $row["idUsers"];
					$_SESSION["firstName"] = $row["firstName"];
					$_SESSION["lastName"] = $row["lastName"];
					$_SESSION["email"] = $row["email"];
					$_SESSION["avatar"] = $row["avatar"];
					$_SESSION["password"] = $password;
					$_SESSION["type"] = $row["type"];
					$_SESSION["address"] = $row["address"];
					$_SESSION["phone"] = $row["phone"];
				}
			}
			else
			{
				header('Location: http://localhost/eStore/Login.php?error=1');
			}
		}
		$cart = 'SELECT * FROM cart WHERE Users_idUsers="'.$_SESSION["idUsers"].'" and shipped="0"';
		if ($result=mysqli_query($conn,$cart)) {
			if(mysqli_num_rows($result) > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$_SESSION["idCart"] = $row["idCart"];
				}
			}
			else
			{
				$cartCreate = 'INSERT INTO cart(Users_idUsers) VALUES("'.$_SESSION["idUsers"].'")';
				mysqli_query($conn,$cartCreate);
				$cart = 'SELECT * FROM cart WHERE Users_idUsers="'.$_SESSION["idUsers"].'" and shipped="0"';
				$result=mysqli_query($conn,$cart);
				$row = $result->fetch_assoc();
				$_SESSION["idCart"] = $row["idCart"];
			}
		}
	}
	$conn->close();
	?>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" id = "logo" class="img-circle"></center><br>
	<div class="container">
		<hr>
		<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "estore";

// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
		if ($conn->connect_error)
		{
			die("Connection failed: " . $conn->connect_error);
		}
		$query = 'SELECT * FROM product';
		$result=mysqli_query($conn,$query) or die(mysqli_error($conn));
		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_assoc())
			{
				if($row["quantity"]=='0')
					$stock = '<button class="btn btn-default glyphicon glyphicon-ban-circle" disabled> Out of stock.</button>';
				else
					$stock = '<form role="form" action="cart.php" method="post"><input type="text" class="hidden" name="product" value="'.$row['idProduct'].'"><input type="text" class="hidden" name="quantity" value="'.$row['quantity'].'"><button type="submit" class="btn btn-default glyphicon glyphicon-shopping-cart"> Add to cart.</button></form>';

				echo '<div class="col-md-3" id ="border">
				<div class="row">
					<div class="col-md-12">
						<center><img id = "product" src="uploads/'.$row["image"].'">
							<h3 class="bg-success">'.$row["name"].'</h3></center>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<center>'.$stock.'</center>
						</div>
						<div class="col-md-6">
							<center><h4><strong>'.$row["price"].' EGP</strong></h4></center>
						</div>
					</div>
				</div>';
			}
		}
		$conn->close();
		?>
	</div>
	<div class="container"><hr><div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
