<!DOCTYPE html>
<style type="text/css">
	img{
		max-width: 10%;
		max-height: 10%;
	}
</style>
<html>
<head>
	<title>Register</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" class="img-circle"></center><br>
	<div class="container">
		<hr>
		<?php if(!empty($_GET['mail'])) echo "<div class='alert alert-danger'><strong>Error!</strong> Email already used.</div>"; ?>
		<form role="form" action="Login.php" method="post" enctype="multipart/form-data" id ="myform">
			<center>
				<div class="form-group">
					<label for="email">Avatar</label><br>
					<span class="btn btn-default btn-file">
						Browse <input type="file" name = "avatar">
					</span>
				</div>
			</center>
			<div class="col-md-6">
				<div class="form-group">
					<label for="email">First Name*</label>
					<input type="text" class="form-control" name="firstName" required value='<?php if(!empty($_SESSION)) echo $_SESSION["first"]; ?>'>
				</div>
				<div class="form-group">
					<label for="email">Last Name*</label>
					<input type="text" class="form-control" name="lastName" required value='<?php if(!empty($_SESSION)) echo $_SESSION["last"]; ?>'>
				</div>
				<div class="form-group">
					<label for="email">Address*</label>
					<input type="text" class="form-control" name="address" required value='<?php if(!empty($_SESSION)) echo $_SESSION["add"]; ?>'>
				</div>
				<div class="form-group">
					<label for="phone">Phone*</label>
					<input type="number" class="form-control" name="phone" required value='<?php if(!empty($_SESSION)) echo $_SESSION["phone"]; ?>'>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="pwd">Email*</label>
					<input type="email" class="form-control" name="email" required value='<?php if(!empty($_SESSION)) echo $_SESSION["mail"]; ?>'>
				</div>
				<div class="form-group">
					<label for="pwd">Password*</label>
					<input type="password" class="form-control" name="password" id="password" required>
				</div>
				<div class="form-group">
					<label for="pwd">Confirm Password*</label>
					<input type="password" class="form-control" name="conpassword" id='conpassword' required>
				</div>
			</div>

			<div class="col-md-12"><center><button type="submit" class="btn btn-primary">Register</button></center>
				<h6>* Required Fields</h6>
				<a href="Login.php"><p align="right">Already have an account? Login</p></a></div>
			</form>
		</div>
		<script src="https://code.jquery.com/jquery.js"></script>
		<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
		<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			jQuery.validator.setDefaults({
			});
			$( "#myform" ).validate({
				rules: {
					password: "required",
					conpassword: {
						equalTo: "#password"
					}
				}
			});
		</script>
	</body>
	</html>
