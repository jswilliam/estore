<!DOCTYPE html>
<?php session_start();  ?>
<style type="text/css">
	img{
		max-width: 10%;
		max-height: 10%;
	}
	.left {
		float: left;
	}
	.right {
		float: right;
	}
</style>
<html>
<head>
	<title>Cart</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" class="img-circle"></center>
	<div class="container">
		<hr>
		<h2 class="text-primary">Cart - Confirmation</h2>
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php
					$servername = "localhost";
					$username = "root";
					$password = "";
					$dbname = "estore";

// Create connection
					$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
					if ($conn->connect_error)
					{
						die("Connection failed: " . $conn->connect_error);
					}
					if(empty($_SESSION['idCart']))
						$_SESSION['idCart']='x';
					$query = 'SELECT * FROM Product_has_Cart c, product p WHERE c.Cart_idCart ="'.$_SESSION['idCart'].'" AND p.idProduct = c.Product_idProduct';
					$result=mysqli_query($conn,$query) or die(mysqli_error($conn));
					if(mysqli_num_rows($result) > 0)
					{
						while($row = $result->fetch_assoc())
						{
							echo '<span class="left">'.$row['name'].'</span><span class="right">EGP '.$row['price'].' <a href="remove.php?idCartProduct='.$row['idCartProduct'].'&quantity='.$row['quantity'].'&idProduct='.$row['idProduct'].'" class="glyphicon glyphicon-remove"></a></span>​<br>';
						}
						echo '<hr>';
						echo '<span class="left">Shipping</span><span class="right"><strong>EGP 25</strong></span><br>';
						$total = 'SELECT SUM(p.price) as total ,c.Cart_idCart FROM Product_has_Cart c, product p WHERE c.Cart_idCart = "'.$_SESSION['idCart'].'" AND p.idProduct = c.Product_idProduct';
						$result1=mysqli_query($conn,$total);
						$row = $result1->fetch_assoc();
						$finalTotal = $row['total'] + 25;
						$_SESSION['cartTotal'] = $finalTotal;

						//Cart products
						$cartProducts = 'SELECT p.name ,c.Cart_idCart FROM Product_has_Cart c, product p WHERE c.Cart_idCart = "'.$_SESSION['idCart'].'" AND p.idProduct = c.Product_idProduct';
						$result2=mysqli_query($conn,$cartProducts);
						if(mysqli_num_rows($result2) > 0)
						{
							$cartProducts = "";
							while($row2 = $result2->fetch_assoc())
							{
								$cartProducts .= $row2['name'] . " - ";
							}
							$cartProducts = substr($cartProducts, 0, -2);
							$_SESSION['cartProducts'] = $cartProducts;
						}
						if(mysqli_num_rows($result) > 0)
						{
							echo '<span class="right"><strong>EGP '.$finalTotal.'</strong></span><br><br>';
							echo '<form class="right" role="form" action="checkout.php" method="post"><input type="text" class="hidden" name="cart" value="'.$row['Cart_idCart'].'"><button type="submit" class="btn btn-default glyphicon glyphicon-ok"> Checkout</button></form><br><br>';
						}
						echo "<a href='Products.php'><p align='right'>Continue shopping</p></a>";
					}
					else
					{
						echo "<center><h3>Your cart is empty.</h3></center>";
						echo "<a href='Products.php'><p align='right'>Check our products.</p></a>";
					}
					$conn->close();
					?>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
