<?php
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "estore";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error)
{
	die("Connection failed: " . $conn->connect_error);
}
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["avatar"]["name"]);
$uploadOk = 1;
$avatar = "default.png";
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
{
	echo "<div class='alert alert-danger'><strong>Error!</strong>Only JPG, JPEG, PNG & GIF files are allowed.</div>";
	$uploadOk = 0;
}
	// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0)
{
	echo "<div class='alert alert-danger'><strong>Error!</strong>Your file was not uploaded.</div>";
	// if everything is ok, try to upload file
}
else
{
	if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file))
	{
		$avatar = basename($_FILES["avatar"]["name"]);
	}
	else
	{
		$avatar = "default.png";
	}
}

$sql = 'UPDATE users SET avatar="'.$avatar.'" WHERE idUsers="'.$_SESSION["idUsers"].'"';
if ($conn->query($sql) === TRUE) {
	$_SESSION['avatar'] = $avatar;
	$conn->close();
	header('Location: http://localhost/eStore/profile.php');
}
else
{
	$_SESSION['avatar'] = $avatar;
	$conn->close();
	header('Location: http://localhost/eStore/profile.php');
}

?>
