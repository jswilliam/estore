<!DOCTYPE html>
<?php session_start();  ?>
<style type="text/css">
	img{
		max-width: 10%;
		max-height: 10%;
	}
	.left {
		float: left;
	}
	.right {
		float: right;
	}
</style>
<html>
<head>
	<title>Cart</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" class="img-circle"></center>
	<div class="container">
		<hr>
		<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "estore";

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error)
		{
			die("Connection failed: " . $conn->connect_error);
		}
		$shipped = "UPDATE cart SET shipped='1' WHERE idCart='".$_POST['cart']."'";
		mysqli_query($conn,$shipped);
		$history = "SELECT DISTINCT p.idProduct AS idProduct FROM cart_has_products c, product p WHERE c.idCart = '".$_POST['cart']."' AND c.idProduct = p.idProduct";
		if ($result=mysqli_query($conn,$history))
		{
			if(mysqli_num_rows($result) > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$addHistory = "INSERT INTO product_has_users(Product_idProduct,Users_idUsers)VALUES('".$row['idProduct']."','".$_SESSION['idUsers']."')";
					mysqli_query($conn,$addHistory);
				}
			}
		}
		echo
		"<div class='alert alert-success'><center>
		<strong>Thank you!</strong> Your order is on its way to ".$_SESSION['address']."
	</center></div>";

	$cartCreate = 'INSERT INTO cart(Users_idUsers) VALUES("'.$_SESSION["idUsers"].'")';
	mysqli_query($conn,$cartCreate);
	$cart = 'SELECT * FROM cart WHERE Users_idUsers="'.$_SESSION["idUsers"].'" and shipped="0"';
	$result=mysqli_query($conn,$cart);
	$row = $result->fetch_assoc();


	//Send cart confirmation mail
	require_once(dirname(__FILE__)."/vendor/swiftmailer/swiftmailer/lib/swift_required.php");
	$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
	->setUsername('jswilliam93')
	->setPassword('Fourzeroes');

	$mailer = Swift_Mailer::newInstance($transport);

	$message = Swift_Message::newInstance('Order Confirmation')
	->setFrom(array('jswilliam93@gmail.com' => 'Joe William'))
	->setTo(array('jswilliam93@gmail.com'))
	->setBody('Hi '.$_SESSION['firstName']." ".$_SESSION['lastName']." - ".$_SESSION['email']." - ,\n".'This is to confirm that order #'.$_SESSION["idCart"]." ( ".$_SESSION['cartProducts']." ) "." total of EGP ".$_SESSION['cartTotal']. " to ".$_SESSION['address']." and Phone: ".$_SESSION['phone']." will be shipped soon.");

	$_SESSION["idCart"] = $row["idCart"];
	$_SESSION['cartTotal'] = 0;

	$result = $mailer->send($message);
	$conn->close();
	?>

</div>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
