<!DOCTYPE html>
<?php session_start(); ?>
<style type="text/css">
	img{
		max-width: 10%;
		max-height: 10%;
	}
</style>
<html>
<head>
	<title>Edit Profile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" class="img-circle"></center><br>
	<div class="container">
		<hr>
		<form role="form" action="edit_profile.php" method="post" enctype="multipart/form-data">
			<div class="col-md-6">
				<div class="form-group">
					<label for="email">First Name</label>
					<input type="text" class="form-control" name="firstName" value="<?php  echo $_SESSION["firstName"];?>">
				</div>
				<div class="form-group">
					<label for="email">Last Name</label>
					<input type="text" class="form-control" name="lastName" value="<?php  echo $_SESSION["lastName"];?>">
				</div>
				<div class="form-group">
					<label for="email">Address*</label>
					<input type="text" class="form-control" name="address" value="<?php  echo $_SESSION["address"];?>" required>
				</div>
			</div>
			<div class="col-md-6">
			<div class="form-group">
					<label for="phone">Phone</label>
					<input type="number" class="form-control" name="phone" value="<?php  echo $_SESSION["phone"];?>">
				</div>
				<div class="form-group">
					<label for="pwd">Email</label>
					<input type="email" class="form-control" name="email" value="<?php  echo $_SESSION["email"];?>">
				</div>
				<div class="form-group">
					<label for="pwd">Password</label>
					<input type="password" class="form-control" name="password" value="<?php  echo $_SESSION["password"];?>">
				</div>
			</div>
			<div class="col-md-12">
				<center><button type="submit" class="btn btn-primary">Edit</button></center>
			</div>
		</form>
	</div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
