<!DOCTYPE html>
<?php session_start(); ?>
<style type="text/css">
	img{
		max-width: 10%;
		max-height: 10%;
	}
	.profile{
		max-width: 30%;
		max-height: 30%;
	}
</style>
<html>
<head>
	<title>Profile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="logo.jpg">
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<br>
	<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="products.php">eStore.eg</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="glyphicon glyphicon-shopping-cart" href="cart_view.php"></a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if(!empty($_SESSION["firstName"])) echo $_SESSION["firstName"] ." ".$_SESSION["lastName"]; else echo "Guest"?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "profile.php"; else echo "login.php?guest=1"?>">Profile</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "edit.php"; else echo "login.php?guest=1"?>">Edit Profile</a></li>
							<li><a class="<?php if($_SESSION["type"]=="Buyer") echo "hidden" ?>"href="<?php if(!empty($_SESSION["firstName"])) echo "add_product_view.php"; else echo "login.php?guest=1"?>">Add Product</a></li>
							<li><a href="<?php if(!empty($_SESSION["firstName"])) echo "history.php"; else echo "login.php?guest=1"?>">History</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="Logout.php"><?php if(empty($_SESSION["firstName"])) echo "Login"; else echo "Logout";?></a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<center><img src="logo.jpg" class="img-circle"></center><br>
	<div class="container">
	<hr>
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-body">
					<center>
						<img src="uploads/<?php echo $_SESSION['avatar'] ?>" class="profile img-rounded"><br><br>
						<form action="change_avatar.php" method="post" role="form" enctype="multipart/form-data">
							<div class="form-group">
								<span class="btn btn-default btn-file">
									Change avatar. <input type="file" name = "avatar">
								</span>
								<button type="submit" class="btn btn-default">Upload</button>
							</div>
						</form>
					</center><br>
					<h3>Name <h5 class="text-primary"><?php echo $_SESSION['firstName']." ". $_SESSION['lastName']?></h5></h3>
					<h3>Email <h5 class="text-primary"><?php echo $_SESSION['email']?></h5></h3>
					<h3>Address <h5 class="text-primary"><?php echo $_SESSION['address']?></h5></h3>
					<h3>Phone <h5 class="text-primary"><?php echo $_SESSION['phone']?></h5></h3>
					<h3>Account Type <h5 class="text-primary"><?php echo $_SESSION['type']?></h5></h3>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
